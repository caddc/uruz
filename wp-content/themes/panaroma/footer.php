<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package SKT Panaroma
 */
?>


  </div><!-- wrapper -->	
	<footer id="colophon" class="site-footer" role="contentinfo">
    	<div class="foot_col_container"> 
            <div class="contact"><h2><?php _e('Contact Info','panaroma'); ?></h2>
                 <h3 class="company-title"><?php echo esc_html( of_get_option('contact1', true) ); ?></h3>
                 <p><?php echo esc_html( of_get_option('contact2', true) ); ?> <?php echo esc_html( of_get_option('contact3', true) ); ?></p>
                 <?php if(of_get_option('contact4', true) != ''){ ?>
                 <p><strong><?php _e('Phone','panaroma'); ?> :</strong> <?php echo esc_html( of_get_option('contact4', true) ); ?></p>
                 <?php } ?>
                 <?php if(of_get_option('contact5', true) != '') { ?>
                 <p><strong><?php _e('Email','panaroma'); ?> :</strong> <a href="mailto:<?php echo sanitize_email( of_get_option('contact5', true) ); ?>"><?php echo sanitize_email( of_get_option('contact5', true) ); ?></a></p>
                 <?php } ?>
            </div><!-- contact -->
            <div class="clear"></div>
        </div>
	</footer><!-- #colophon -->
  <div class="footer-bottom">
	  <div class="foot_col_container">
        <div class="bottom-left">
        	<?php
			if ( (function_exists( 'of_get_option' ) && (of_get_option('footertext2', true) != 1) ) ) {
			 	echo esc_html( of_get_option('footertext2', true) ); 
			} ?>
        </div><!-- bottom-left -->    
        <div class="bottom-right">
			<?php echo esc_html('');?>
		</div><!-- bottom-right --><div class="clear"></div>
        </div><!-- footer-bottom -->
        </div>
	</div>
</div><!-- #page -->



<?php wp_footer(); ?>
</body>
</html>